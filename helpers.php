<?php

if (!function_exists('mb_hebrev')) {
    /**
     * Reverse a utf-8 string.
     *
     * @param $text
     *
     * @return string
     */
    function mb_hebrev($text)
    {
        preg_match_all('/./us', $text, $ar);

        return join('', array_reverse($ar[ 0 ]));
    }
}

if (!function_exists('px_to_pt')) {
    /**
     * Convert pixels to points.
     *
     * @param int $px
     *
     * @return int
     */
    function px_to_pt(int $px)
    {
        return ceil($px * 0.75);
    }
}

if (!function_exists('calculate_distance')) {
    /**
     * Calculate distance between two coordinates, using haversine formula.
     *
     * @param $fromLatitude
     * @param $fromLongitude
     * @param $toLatitude
     * @param $toLongitude
     *
     * @return float|int
     */
    function calculate_distance($fromLatitude, $fromLongitude, $toLatitude, $toLongitude)
    {
        $earthRadius = 6371;

        $latitudeDistance = deg2rad($toLatitude - $fromLatitude);
        $longitudeDistance = deg2rad($toLongitude - $fromLongitude);

        $a = sin($latitudeDistance / 2) * sin($latitudeDistance / 2) +
            cos(deg2rad($fromLatitude)) * cos(deg2rad($toLatitude)) *
            sin($longitudeDistance / 2) * sin($longitudeDistance / 2);
        $c = 2 * asin(sqrt($a));
        $distanceInMeters = $earthRadius * $c * 1000;

        return $distanceInMeters;
    }
}

if (!function_exists('s3_url')) {
    /**
     * Concatenate s3 bucket url for readability.
     *
     * @param string $path
     *
     * @return float|int
     */
    function s3_url(string $path)
    {
        return rtrim(config('filesystems.disks.s3.url'), '/') . '/' . $path;
    }
}

if (!function_exists('seconds_to_hours')) {
    /**
     * Convert seconds into hours, formatted with x decimals.
     *
     * @param int $seconds
     * @param int $decimals
     *
     * @return float
     */
    function seconds_to_hours(int $seconds, int $decimals = 2): float
    {
        return number_format($seconds / 60 / 60, $decimals);
    }
}