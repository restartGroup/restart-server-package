<?php

namespace RestartPackage\Services;

use Exception;
use Illuminate\Support\Collection;
use RestartPackage\Models\AbstractModel;
use RestartPackage\Repositories\AbstractRepository;

abstract class RepositoryService
{

    /** @var  AbstractRepository */
    protected $repository;

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $attributes
     *
     * @return bool
     */
    public function exists(array $attributes): bool
    {
        return $this->repository->exists($attributes);
    }

    /**
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        return $this->repository->all($columns);
    }

    /**
     * @param array $data
     *
     * @return AbstractModel
     */
    public function create(array $data): AbstractModel
    {
        return $this->repository->create($data);
    }

    /**
     * @param array $data
     * @param mixed $id
     *
     * @return AbstractModel|bool
     */
    public function update(array $data, $id)
    {
        try {
            $model = $this->repository->update($data, $id);
        } catch (Exception $exception) {
            return null;
        }

        return $model;
    }

    /**
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param AbstractModel $model
     * @param string $key
     *
     * @return AbstractModel
     */
    public function updateModel(AbstractModel $model, string $key = '')
    {
        if (empty($key)) {
            $key = $model->id;
        }

        return $this->update($model->attributesToArray(), $key);
    }

    /**
     * @param array|Collection $values
     *
     * @return bool
     */
    public function massCreate($values): bool
    {
        return $this->repository->massCreate($values);
    }

    /**
     * @param array|Collection $ids
     *
     * @return bool|null
     * @throws Exception
     */
    public function massDelete($ids)
    {
        return $this->repository->massDelete($ids);
    }

}