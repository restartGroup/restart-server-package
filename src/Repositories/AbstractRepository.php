<?php

namespace RestartPackage\Repositories;

use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use RestartPackage\Models\AbstractModel;

abstract class AbstractRepository extends BaseRepository
{

    /** @var  AbstractModel */
    protected $model;

    /**
     * Check if supplied attributes already exists.
     *
     * @param array $attributes
     *
     * @return bool
     */
    public function exists(array $attributes): bool
    {
        return $this->model->where($attributes)->exists();
    }

    /**
     * @param array|Collection $values
     *
     * @return bool
     */
    public function massCreate($values): bool
    {
        if ($values instanceof Collection) {
            $values = $values->toArray();
        }

        return $this->model->insert($values);
    }

    /**
     * @param array|Collection $ids
     *
     * @throws \Exception
     *
     * @return bool|null
     */
    public function massDelete($ids)
    {
        if ($ids instanceof Collection) {
            $ids = $ids->toArray();
        }

        return $this->model->whereIn('id', $ids)->delete();
    }

    /**
     * Apply criteria in current Query
     * todo: remove after bug fixing in l5-repository https://github.com/andersao/l5-repository/issues/267
     *
     * @return $this
     */
    protected function applyCriteria()
    {
        if ($this->skipCriteria === true) {
            return $this;
        }

        $criteria = $this->getCriteria();
        $query = $this->model->newQuery();

        if ($criteria) {
            foreach ($criteria as $c) {
                if ($c instanceof CriteriaInterface) {
                    $query = $c->apply($query, $this);
                }
            }
        }

        return $query;
    }

    /**
     * Find data by Criteria
     * todo: remove after bug fixing in l5-repository https://github.com/andersao/l5-repository/issues/267
     *
     * @param CriteriaInterface $criteria
     *
     * @return mixed
     */
    public function getByCriteria(CriteriaInterface $criteria)
    {
        $this->model = $criteria->apply($this->model->newQuery(), $this);
        $results = $this->model->get();
        $this->resetModel();

        return $this->parserResult($results);
    }

    /**
     * Retrieve all data of repository, paginated.
     *
     * @param null $limit
     * @param array $columns
     * @param string $method
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate")
    {
        $limit = $this->app[ 'request' ]->input(config('repository.criteria.params.perPage'));

        if (filter_var($limit, FILTER_VALIDATE_INT) !== false && (int)$limit >= 1) {
            return parent::paginate((int)$limit, $columns);
        }

        return parent::paginate((int)$limit, $columns);
    }

}