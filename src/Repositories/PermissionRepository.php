<?php

namespace RestartPackage\Repositories;


use Spatie\Permission\Models\Permission;

class PermissionRepository extends AbstractRepository
{
    public function model()
    {
       return Permission::class;
    }

}