<?php
/**
 * Created by PhpStorm.
 * User: eran
 * Date: 11/01/17
 * Time: 15:17
 */

namespace RestartPackage\Repositories;


use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Role;

class RoleRepository extends AbstractRepository
{
    public function model()
    {
        return Role::class;
    }

    /**
     * @param Role $role
     * @param Permission $permission
     */
    public function givePermission(Role $role, Permission $permission)
    {
        $role->givePermissionTo($permission);
    }

    /**
     * @param Role $role
     * @param Permission $permission
     * @return bool
     */
    public function hasPermission(Role $role, Permission $permission)
    {
        return $role->hasPermissionTo($permission);
    }

    /**
     * @param Role $role
     * @param Permission $permission
     */
    public function revokePermission(Role $role, Permission $permission)
    {
        $role->revokePermissionTo($permission);
    }



}