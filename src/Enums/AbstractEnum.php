<?php

namespace RestartPackage\Enums;

use ReflectionClass;

abstract class AbstractEnum
{

    private static $constCacheArray = null;

    /**
     * @return array
     */
    public static function getConstants(): array
    {
        if (self::$constCacheArray == null) {
            self::$constCacheArray = [];
        }

        $calledClass = get_called_class();

        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[ $calledClass ] = $reflect->getConstants();
        }

        return self::$constCacheArray[ $calledClass ];
    }

    /**
     * @param $name
     * @param bool $strict
     *
     * @return bool
     */
    public static function isValidName($name, $strict = true): bool
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));

        return in_array(strtolower($name), $keys);
    }

    /**
     * @param $value
     * @param bool $strict
     *
     * @return bool
     */
    public static function isValidValue($value, $strict = true): bool
    {
        $values = array_values(self::getConstants());

        return in_array($value, $values, $strict);
    }

    /**
     * @param string $name
     *
     * @return int
     */
    public static function constByName(string $name): int
    {
        if (!self::isValidName($name)) {
            return false;
        }

        $calledClass = get_called_class();

        return self::$constCacheArray[ $calledClass ][ $name ];
    }

    /**
     * @param int $const
     *
     * @return string
     */
    public static function nameByConst(int $const): string
    {
        if (!self::isValidValue($const)) {
            return false;
        }

        $calledClass = get_called_class();

        return array_search($const, self::$constCacheArray[ $calledClass ]);
    }

}