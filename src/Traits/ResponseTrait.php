<?php

namespace RestartPackage\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator as BaseLengthAwarePaginator;
use RestartPackage\Pagination\LengthAwarePaginator;
use stdClass;

/**
 * Class ResponseTrait
 *
 * @package RestartPackage\Traits
 */
trait ResponseTrait
{

    /**
     * @param string|array $data
     * @param int $statusCode
     * @param string $id
     *
     * @return JsonResponse
     */
    protected function respondWithError(
        $data = '',
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        string $id = ''
    ): JsonResponse {
        $errors = [];

        // Most of the time it will be used by the validation exception that returns an array
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $errors[] = $this->makeErrorRow('', $key, array_pop($value));
                } else {
                    $errors[] = $this->makeErrorRow('', $key, $value);
                }
            }
        }

        // Format most of the exceptions and custom error responses from controllers
        if (is_string($data)) {
            $title = trans("{$data}_title");
            $message = trans($data);

            if (empty($title) || $title === "{$data}_title") {
                $title = Response::$statusTexts[ $statusCode ];
            }

            $errors = [ $this->makeErrorRow($id, $title, $message) ];
        }

        return response()->json([
            'errors' => $errors,
            'status' => $statusCode,
        ], $statusCode);
    }

    /**
     * @param string|array $errors
     * @param int $statusCode
     *
     * @return JsonResponse
     */
    protected function respondWithErrors(
        array $errors,
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse {
        return $this->respondWithError($errors, $statusCode);
    }

    /**
     * @param string $id
     * @param string $title
     * @param string $message
     *
     * @return stdClass
     */
    protected function makeErrorRow(string $id, string $title, string $message)
    {
        $error = new stdClass();

        if (!empty($id)) {
            $error->id = $id;
        }

        $error->title = $title;
        $error->message = $message;

        return $error;
    }

    /**
     * @param mixed $data
     *
     * @param int $statusCode
     * @return JsonResponse
     */
    protected function respond($data = '', int $statusCode = Response::HTTP_OK): JsonResponse
    {
        // todo: what to do?
        if ($data instanceof BaseLengthAwarePaginator) {
            $data = new LengthAwarePaginator($data);
        }

        return response()->json([
            'errors' => false,
            'data'   => $data,
        ], $statusCode);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function unauthorized($data)
    {
        return $this->respondWithError($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function forbidden($data)
    {
        return $this->respondWithError($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function notFound($data)
    {
        return $this->respondWithError($data, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function unprocessableEntity($data)
    {
        return $this->respondWithError($data, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function conflict($data)
    {
        return $this->respondWithError($data, Response::HTTP_CONFLICT);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function tooManyRequests($data)
    {
        return $this->respondWithError($data, Response::HTTP_TOO_MANY_REQUESTS);
    }

    /**
     * @param string|array $data
     *
     * @return JsonResponse
     */
    protected function badRequest($data)
    {
        return $this->respondWithError($data, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param mixed $data
     *
     * @return JsonResponse
     */
    protected function ok($data = ''): JsonResponse
    {
        return $this->respond($data);
    }

}