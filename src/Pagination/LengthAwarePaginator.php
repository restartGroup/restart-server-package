<?php

namespace RestartPackage\Pagination;

use Illuminate\Pagination\LengthAwarePaginator as BaseLengthAwarePaginator;

class LengthAwarePaginator extends BaseLengthAwarePaginator
{
    /**
     * CustomLengthAwarePaginator constructor.
     *
     * @param BaseLengthAwarePaginator $lengthAwarePaginator
     */
    public function __construct(BaseLengthAwarePaginator $lengthAwarePaginator)
    {
        $this->total = $lengthAwarePaginator->total;
        $this->perPage = $lengthAwarePaginator->perPage;
        $this->lastPage = $lengthAwarePaginator->lastPage;
        $this->path = $lengthAwarePaginator->path;
        $this->currentPage = $lengthAwarePaginator->currentPage;
        $this->items = $lengthAwarePaginator->items;
        $this->path = $lengthAwarePaginator->path;
        $this->query = $lengthAwarePaginator->query;
        $this->fragment = $lengthAwarePaginator->fragment;
        $this->pageName = $lengthAwarePaginator->pageName;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $convertedArray = [];

        foreach (parent::toArray() as $key => $value) {
            $convertedArray[ camel_case($key) ] = $value;
        }

        return $convertedArray;
    }

}