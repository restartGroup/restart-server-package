<?php

namespace RestartPackage\Validation;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Validation\Validator as BaseValidator;

class Validator extends BaseValidator
{

    const REGEX_DECIMAL = '/^\d{1,%s}(\.\d{1,%s})?$/';
    const REGEX_BASE64 = '/^data:image\/(.*);base64/';
    const REGEX_PHONE = '/^0(([2-489]\d{7})|([5|7]\d\d{7}))|1(80\d{7}|(700|599)\d{6})$/';
    const REGEX_MOBILE_PHONE = '/^05\d\d{7}$/';
    const REGEX_PASSWORD = '/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/';
    const REGEX_SIMPLE_PASSWORD = '/(?=.*[a-zA-Z])(?=.*[0-9])(?=.{6,})/';
    const REGEX_LATITUDE_REGEX = '/^[-]?((([0-8]?[0-9])(\.(\d+))?)|(90(\.0+)?))$/';
    const REGEX_LONGITUDE_REGEX = '/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))(\.(\d+))?)|180(\.0+)?)$/';
    const ALLOWED_IMAGE_TYPE = ['jpeg', 'png', 'gif', 'bmp', 'svg'];
    const DATE_FORMAT_SAME_MONTH = 'Y-m';
    const DATE_FORMAT_SAME_YEAR = 'Y';

    /**
     * Validate that an attribute has a matching confirmation.
     *
     * @param  string $attribute
     * @param  mixed $value
     *
     * @return bool
     */
    public function validateConfirmed($attribute, $value)
    {
        return $this->validateSame($attribute, $value, [$attribute . 'Confirmation']);
    }

    /**
     * Validate that an attribute is a decimal.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @param  array $parameters
     *
     * @return bool
     */
    public function validateDecimal($attribute, $value, $parameters)
    {
        if (!is_numeric($value)) {
            return false;
        }

        $this->requireParameterCount(2, $parameters, 'decimal');

        $regex = sprintf(self::REGEX_DECIMAL, $parameters[ 0 ], $parameters[ 1 ]);

        return preg_match($regex, $value) > 0;
    }

    /**
     * Replace all place-holders for the decimal rule.
     *
     * @param  string $message
     * @param  string $attribute
     * @param  string $rule
     * @param  array $parameters
     *
     * @return string
     */
    public function replaceDecimal($message, $attribute, $rule, $parameters)
    {
        $max = sprintf('%d.%d', str_repeat('9', $parameters[ 0 ]), str_repeat('9', $parameters[ 1 ]));

        return str_replace(':max', $max, $message);
    }

    /**
     * Validate that an attribute is an image encoded with base64.
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validateBase64Image($attribute, $value): bool
    {
        preg_match(self::REGEX_BASE64, $value, $match);

        return isset($match[ 1 ]) && in_array($match[ 1 ], self::ALLOWED_IMAGE_TYPE);
    }

    /**
     * Validate that an attribute is an array with numeric values.
     *
     * @param $attribute
     * @param $values
     *
     * @return bool
     */
    public function validateNumericArray($attribute, $values): bool
    {
        if (!is_array($values)) {
            return false;
        }

        foreach ($values as $value) {
            if (!is_numeric($value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate that an attribute is a phone number.
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validatePhone($attribute, $value): bool
    {
        return preg_match(self::REGEX_PHONE, $value) > 0;
    }

    /**
     * Validate that an attribute is a mobile phone number.
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validateMobilePhone($attribute, $value): bool
    {
        return preg_match(self::REGEX_MOBILE_PHONE, $value) > 0;
    }

    /**
     * Validate that an attribute is a password.
     * (8 characters, at least one digit, one uppercase letter and one lowercase letter)
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validatePassword($attribute, $value): bool
    {
        return preg_match(self::REGEX_PASSWORD, $value) > 0;
    }

    /**
     * Validate that an attribute is a simple password.
     * (6 characters, at least one digit and one letter)
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validateSimplePassword($attribute, $value): bool
    {
        return preg_match(self::REGEX_SIMPLE_PASSWORD, $value) > 0;
    }

    /**
     * Validates that an attribute is a latitude.
     *
     * @param float|int|string $lat Latitude
     *
     * @return bool `true` if $lat is valid, `false` if not
     */
    public function validateLatitude($attribute, $value)
    {
        return preg_match(self::REGEX_LATITUDE_REGEX, $value);
    }

    /**
     * Validates that an attribute is a longitude.
     *
     * @param float|int|string $lng Longitude
     *
     * @return bool `true` if $long is valid, `false` if not
     */
    public function validateLongitude($attribute, $value)
    {
        return preg_match(self::REGEX_LONGITUDE_REGEX, $value);
    }

    /**
     * Validate that an attribute matches a multiple date formats.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @param  array $parameters
     *
     * @return bool
     */
    public function validateMultiDateFormat($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'multi_date_format');

        $formats = explode(';', $parameters[ 0 ]);

        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        foreach ($formats as $format) {
            $parsed = date_parse_from_format($format, $value);

            if ($parsed[ 'error_count' ] === 0 && $parsed[ 'warning_count' ] === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if two values are equal in the supplied date format.
     *
     * @param $value
     * @param $other
     * @param $format
     *
     * @return bool
     */
    private function validateDatesByFormat($value, $other, $format)
    {
        $parsed = Carbon::createFromTimestamp(strtotime($value));
        $parsedOther = Carbon::createFromTimestamp(strtotime($other));

        return $parsed->format($format) === $parsedOther->format($format);
    }

    /**
     * Validate that two attributes are in the same month and year.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    public function validateSameMonth($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'same_date');
        $other = Arr::get($this->data, $parameters[ 0 ]);

        return $this->validateDatesByFormat($value, $other, self::DATE_FORMAT_SAME_MONTH);
    }

    /**
     * Replace all place-holders for the same month rule.
     *
     * @param  string $message
     * @param  string $attribute
     * @param  string $rule
     * @param  array $parameters
     *
     * @return string
     */
    protected function replaceSameMonth($message, $attribute, $rule, $parameters)
    {
        return $this->replaceSame($message, $attribute, $rule, $parameters);
    }

    /**
     * Validate that two attributes are in the same year.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    public function validateSameYear($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'same_year');
        $other = Arr::get($this->data, $parameters[ 0 ]);

        return $this->validateDatesByFormat($value, $other, self::DATE_FORMAT_SAME_YEAR);
    }

    /**
     * Replace all place-holders for the same year rule.
     *
     * @param  string $message
     * @param  string $attribute
     * @param  string $rule
     * @param  array $parameters
     *
     * @return string
     */
    protected function replaceSameYear($message, $attribute, $rule, $parameters)
    {
        return $this->replaceSame($message, $attribute, $rule, $parameters);
    }

}