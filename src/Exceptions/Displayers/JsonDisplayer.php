<?php

namespace RestartPackage\Exceptions\Displayers;

use Exception;
use GrahamCampbell\Exceptions\Displayers\JsonDisplayer as BaseJsonDisplayer;
use Illuminate\Validation\ValidationException;
use RestartPackage\Traits\ResponseTrait;

class JsonDisplayer extends BaseJsonDisplayer
{
    use ResponseTrait;

    /**
     * Get the error response associated with the given exception.
     *
     * @param \Exception $exception
     * @param string $id
     * @param int $code
     * @param string[] $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function display(Exception $exception, $id, $code, array $headers)
    {
        if ($exception instanceof ValidationException) {
            return $this->unprocessableEntity($exception->errors());
        }

        $info = $this->info->generate($exception, $id, $code);

        return $this->respondWithError($info[ 'detail' ], $info[ 'code' ], $id);
    }

}