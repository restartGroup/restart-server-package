<?php

namespace RestartPackage\Models;

use Illuminate\Database\Eloquent\Model;
use RestartPackage\Traits\CamelCaseTrait;

abstract class AbstractModel extends Model
{
    use CamelCaseTrait;

    /**
     * Indicates whether attributes are snake cased on arrays.
     *
     * @var bool
     */
    public static $snakeAttributes = false;

}