<?php

namespace RestartPackage\Providers;

use Arcanedev\LogViewer\LogViewerServiceProvider;
use Barryvdh\Cors\ServiceProvider as CorsServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use GrahamCampbell\Exceptions\ExceptionsServiceProvider;
use Illuminate\Support\Facades\Validator as BaseValidator;
use Illuminate\Support\ServiceProvider;
use Prettus\Repository\Providers\RepositoryServiceProvider;
use RestartPackage\Validation\Validator;
use Spatie\Permission\PermissionServiceProvider;
use Tymon\JWTAuth\Providers\LaravelServiceProvider as JWTServiceProvider;

class RestartPackageProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config' => config_path(),
        ]);

        $this->bootValidator();
    }

    /**
     * Replace laravel's validator with the custom made validator.
     *
     * @return void
     */
    private function bootValidator()
    {
        BaseValidator::resolver(function ($translator, $data, $rules, $messages = [], $customAttributes = []) {
            return new Validator($translator, $data, $rules, $messages, $customAttributes);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerExternalProviders();
    }

    /**
     * Register the bindings for the external providers.
     */
    private function registerExternalProviders()
    {
        $this->app->register(CorsServiceProvider::class);
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->register(JWTServiceProvider::class);
        $this->app->register(PermissionServiceProvider::class);
        $this->app->register(ExceptionsServiceProvider::class);
        $this->app->register(IdeHelperServiceProvider::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(LogViewerServiceProvider::class);
        }
    }

}
