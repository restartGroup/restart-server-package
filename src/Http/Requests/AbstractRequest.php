<?php

namespace RestartPackage\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use RestartPackage\Traits\ResponseTrait;

abstract class AbstractRequest extends FormRequest
{
    use ResponseTrait;

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $status = Response::HTTP_UNPROCESSABLE_ENTITY;

        return $this->respondWithErrors($errors, $status);
    }

    /**
     * Get all the request's input by the rules
     *
     * @return array
     */
    public function inputs(): array
    {
        $inputs = [];

        foreach ($this->rules() as $name => $rule) {
            $input = $this->input($name);

            // Check if exists and it's not an array
            if ((strpos($name, 'boolean') === false && !is_null($input)) &&
                strpos($name, '*') === false && strpos($name, '.') === false
            ) {
                $inputs[ $name ] = $input;
            }
        }

        return $inputs;
    }

}