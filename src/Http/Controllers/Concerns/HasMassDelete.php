<?php

namespace RestartPackage\Http\Controllers\Concerns;

use RestartPackage\Http\Requests\MassDeleteRequest;

trait HasMassDelete
{

    /**
     * Mass Delete
     *
     * @param MassDeleteRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function delete(MassDeleteRequest $request)
    {
        $ids = $request->input('ids');

        $this->service->massDelete($ids);

        return $this->ok();
    }

}