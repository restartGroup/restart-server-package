<?php

namespace RestartPackage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use RestartPackage\Traits\ResponseTrait;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\Authenticate as JWTAuthenticate;

class Authenticate extends JWTAuthenticate
{
    use ResponseTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oldToken = $this->auth->getToken();

        try {
            $this->authenticateAndRefresh($request);
        } catch (UnauthorizedHttpException $exception) {
            return $this->unauthorized('auth.token_invalid');
        } catch (BadRequestHttpException $exception) {
            return $this->unauthorized('auth.token_not_provided');
        }

        $response = $next($request);

        // send the refreshed token back to the client
        $currentToken = $this->auth->getToken();

        if ($currentToken->get() !== $oldToken->get()) {
            $response = $this->setAuthenticationHeader($response, $currentToken);
        }

        return $response;
    }

    /**
     * Attempt to authenticate a user via the token in the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
     *
     * @return string
     */
    public function authenticateAndRefresh(Request $request)
    {
        $this->checkForToken($request);

        try {
            $this->auth->parseToken()->authenticate();
        } catch (TokenExpiredException $e) {
            try {
                $token = $this->auth->parseToken()->refresh();
                $this->auth->setToken($token)->authenticate();

                return $token;
            } catch (JWTException $e) {
                throw new UnauthorizedHttpException('jwt-auth', $e->getMessage(), $e, $e->getCode());
            }
        } catch (JWTException $e) {
            throw new UnauthorizedHttpException('jwt-auth', $e->getMessage(), $e, $e->getCode());
        }
    }

}