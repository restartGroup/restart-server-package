<?php

namespace RestartPackage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use RestartPackage\Traits\ResponseTrait;

class Admin
{
    use ResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        if (!$request->user()->hasRole('admin')) {
            return $this->forbidden('auth.forbidden');
        }

        return $next($request);
    }

}